import json
import numpy as np


def load(path):
    with open(path, "r") as file:
        x = json.load(file)
    x = np.array(x, dtype=np.float64)
    return x


def save(path, x):
    x = x.tolist()
    with open(path, "w") as file:
        json.dump(x, file)

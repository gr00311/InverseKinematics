import numpy as np
import math
import json

# Dictionary IDs for body joints
def get_body_joints_names():

    # Drop joint 4 and joint 7, as these are unnecessary duplicated wrist joints
    joints_names = {
        0: "Head",
        1: "Shoulder_centre",
        2: "L_arm1",
        3: "L_arm2",
        5: "R_arm1",
        6: "R_arm2",
        8: "R_hand0",
        9: "R_hand1",
        10: "R_hand2",
        11: "R_hand3",
        12: "R_hand4",
        13: "R_hand5",
        14: "R_hand6",
        15: "R_hand7",
        16: "R_hand8",
        17: "R_hand9",
        18: "R_hand10",
        19: "R_hand11",
        20: "R_hand12",
        21: "R_hand13",
        22: "R_hand14",
        23: "R_hand15",
        24: "R_hand16",
        25: "R_hand17",
        26: "R_hand18",
        27: "R_hand19",
        28: "R_hand20",
        29: "L_hand0",
        30: "L_hand1",
        31: "L_hand2",
        32: "L_hand3",
        33: "L_hand4",
        34: "L_hand5",
        35: "L_hand6",
        36: "L_hand7",
        37: "L_hand8",
        38: "L_hand9",
        39: "L_hand10",
        40: "L_hand11",
        41: "L_hand12",
        42: "L_hand13",
        43: "L_hand14",
        44: "L_hand15",
        45: "L_hand16",
        46: "L_hand17",
        47: "L_hand18",
        48: "L_hand19",
        49: "L_hand20",
    }

    return joints_names


# Normalise the joints to be uniform across signers
# Normalise with a minus of the neck joint and a divide by the shoulder distance
def normalise_IK_body_joints(joints):

    left_shoulder = joints[2]
    right_shoulder = joints[5]

    # Norm value is the euclidean between left and right shoulder
    norm = math.sqrt(
        ((left_shoulder[0] - right_shoulder[0]) ** 2)
        + ((left_shoulder[1] - right_shoulder[1]) ** 2)
    )

    neck = joints[1]

    # Minus the neck joint
    joints = joints - neck
    # Normalise with the shoulder distance
    joints = joints / norm

    ## NEW
    ## Scaling and translation of the body parameters
    # Scale joints by 240
    joints = joints * 240
    # Translate joints to centre (325,300,0)
    body_offset = [325, 300, 0]
    joints = joints + np.ones((50, 3)) * body_offset

    return joints


# Dictionary IDs for face joints
def get_face_joints_names():

    joints_names = {
        0: "Chin_0",
        1: "Chin_1",
        2: "Chin_2",
        3: "Chin_3",
        4: "Chin_4",
        5: "Chin_5",
        6: "Chin_6",
        7: "Chin_7",
        8: "Chin_8",
        9: "Chin_9",
        10: "Chin_10",
        11: "Chin_11",
        12: "Chin_12",
        13: "Chin_13",
        14: "Chin_14",
        15: "Chin_15",
        16: "Chin_16",
        17: "right_eyebrow_0",
        18: "right_eyebrow_1",
        19: "right_eyebrow_2",
        20: "right_eyebrow_3",
        21: "right_eyebrow_4",
        22: "left_eyebrow_0",
        23: "left_eyebrow_1",
        24: "left_eyebrow_2",
        25: "left_eyebrow_3",
        26: "left_eyebrow_4",
        27: "upper_nose_0",
        28: "upper_nose_1",
        29: "upper_nose_2",
        30: "upper_nose_3",
        31: "lower_nose_0",
        32: "lower_nose_1",
        33: "lower_nose_2",
        34: "lower_nose_3",
        35: "lower_nose_4",
        36: "right_eye_0",
        37: "right_eye_1",
        38: "right_eye_2",
        39: "right_eye_3",
        40: "right_eye_4",
        41: "right_eye_5",
        42: "left_eye_0",
        43: "left_eye_1",
        44: "left_eye_2",
        45: "left_eye_3",
        46: "left_eye_4",
        47: "left_eye_5",
        48: "outer_mouth_0",
        49: "outer_mouth_1",
        50: "outer_mouth_2",
        51: "outer_mouth_3",
        52: "outer_mouth_4",
        53: "outer_mouth_5",
        54: "outer_mouth_6",
        55: "outer_mouth_7",
        56: "outer_mouth_8",
        57: "outer_mouth_9",
        58: "outer_mouth_10",
        59: "outer_mouth_11",
        60: "inner_mouth_0",
        61: "inner_mouth_1",
        62: "inner_mouth_2",
        63: "inner_mouth_3",
        64: "inner_mouth_4",
        65: "inner_mouth_5",
        66: "inner_mouth_6",
        67: "inner_mouth_7",
        68: "right_eye_center",
        69: "left_eye_center",
    }

    return joints_names


# Normalise the face joints, using OpenPose data
# Minus the bottom nose from the face joints
def normalise_face_joints(face_joints, body_head):

    # Minus the bottom nose from the face joints
    bottom_nose = face_joints[27]
    face_joints = face_joints - bottom_nose

    ## NEW
    ## Translation of the face parameters
    # Offset the face by an amount equal to body centre plus 100
    face_offset = [0, 100]
    # Calculate head position
    head = (body_head + np.ones((1, 2)) * face_offset)[0]
    # Scale the face by a factor of 80% - can remove if not required
    face_joints = face_joints * 0.8
    # Minus the chin bottom position, to normalise the face data
    face_joints = face_joints - np.ones((70, 2)) * face_joints[8]
    # Scale back to the head centre - to be 100 above the body centre
    face_joints[:, 0] = face_joints[:, 0] + head[0]
    face_joints[:, 1] = face_joints[:, 1] + head[1]

    return face_joints


# Given joints from either IK for body, or OpenPose for face, create the dictionary to pass to the 3D Model Renderer
def joints_to_dict(joints, mode="body", body_head=None):

    # Body joints should be (50,3)
    if mode == "body":
        joints = normalise_IK_body_joints(joints)
        joints_names = get_body_joints_names()
    # Face joints should be (70,2)
    elif mode == "face":
        joints = normalise_face_joints(joints, body_head)
        joints_names = get_face_joints_names()

    joints_dict = {}
    for (i, joint) in enumerate(joints):

        if i in joints_names:
            joints_dict[joints_names[i]] = [
                "{0:.6f}".format(float(joint[j])) for j in range(len(joint))
            ]

    return joints_dict


# Turn both Ik body joints and OpenPose face joints into a single dictionary to pass to the 3D Model Renderer
def all_joints_to_dict(IK_joints, OpenPose_face_joints):

    # Create IK dictionary
    # IK joints must be (50,3)
    IK_dict = joints_to_dict(np.array(IK_joints), mode="body")

    # Create OpenPose dictionary
    # OpenPose joints must be (70,2)
    # Also give the IK body_head position, to correctly scale the face joints
    OP_face_dict = joints_to_dict(
        np.array(OpenPose_face_joints),
        mode="face",
        body_head=normalise_IK_body_joints(IK_joints)[0, :2],
    )

    all_joints_dict = {}
    all_joints_dict.update(IK_dict)
    all_joints_dict.update(OP_face_dict)

    return all_joints_dict


def create_IK_format_of_OpenPose(OpenPose_frame):

    # OpenPose body format - BODY_25 from https://github.com/CMU-Perceptual-Computing-Lab/openpose/blob/master/doc/output.md
    pose_keypoints_2d = OpenPose_frame["people"][0]["pose_keypoints_2d"]
    pose_keypoints_2d = np.reshape(pose_keypoints_2d, (-1, 3))

    # Subset of OpenPose body needed for IK
    # Head (0), neck(1), Left Shoulder (5), Left Elbow (6), Left Wrist (7), Right Shoulder (2), Right Elbow (3), Right Wrist (4)
    pose_keypoints_2d_IK_subset = pose_keypoints_2d[[0, 1, 5, 6, 7, 2, 3, 4]]

    # OpenPose Right Hand
    hand_right_keypoints_2d = OpenPose_frame["people"][0]["hand_right_keypoints_2d"]
    hand_right_keypoints_2d = np.reshape(hand_right_keypoints_2d, (-1, 3))
    # OpenPose Left Hand
    hand_left_keypoints_2d = OpenPose_frame["people"][0]["hand_left_keypoints_2d"]
    hand_left_keypoints_2d = np.reshape(hand_left_keypoints_2d, (-1, 3))

    frame_data = np.concatenate(
        (
            pose_keypoints_2d_IK_subset.flatten(),
            hand_right_keypoints_2d.flatten(),
            hand_left_keypoints_2d.flatten(),
        ),
        axis=0,
    )

    return frame_data


## NEW
if __name__ == "__main__":

    IK_joints_filename = ""  # Add the IK joints filename here
    IK_joints = json.load(open(IK_joints_filename))

    OpenPose_face_joints_filename = ""  # Add the OpenPose face joints filename here
    OpenPose_face_joints = json.load(open(OpenPose_face_joints_filename))

    frames_dict = {}
    for i in range(len(IK_joints)):

        all_joints_dict = all_joints_to_dict(IK_joints[i], OpenPose_face_joints[i])

        frames_dict[str(i)] = all_joints_dict

    json_filename = ""  # Add the JSON filename here to save
    with open(json_filename, "w") as f:
        json.dump(frames_dict, f, ensure_ascii=False, indent=4, sort_keys=True)

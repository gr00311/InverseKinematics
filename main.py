import h5py
import numpy as np
import json
import torch

from pose_2d import normalize, prune, interpolate
from skeleton import Skeleton
from pose_2d_to_3d import initialize
from pose_3d import Optimization

from utils import save

if __name__ == "__main__":
    from time import time

    start = time()
    data_path = "data/demo-sequence.h5"
    with h5py.File(data_path, "r") as f:
        pose_2d = np.array(f.get("20161025_pocasi"), dtype=np.float64)
    end = time()
    print(f"Loading: {end - start}")

    T, N3 = pose_2d.shape
    assert N3 % 3 == 0
    N = N3 // 3
    pose_2d = pose_2d.reshape((T, N, 3))

    x, y, w = pose_2d[..., 0], pose_2d[..., 1], pose_2d[..., 2]
    save("results/c0.json", np.stack([x, y, w], axis=-1))

    start = time()
    x, y = normalize(x, y)
    end = time()
    print(f"Normalize: {end - start}")
    save("results/c0_c1.json", np.stack([x, y, w], axis=-1))

    start = time()
    x, y, w = prune(x, y, w, (0, 1, 2, 3, 4, 5, 6, 7), 0.3)
    end = time()
    print(f"Pruning: {end - start}")
    save("results/c1_c2.json", np.stack([x, y, w], axis=-1))

    start = time()
    x, y = interpolate(x, y, w, 0.99)
    end = time()
    print(f"Interpolating: {end - start}")
    save("results/c2_c3.json", np.stack([x, y, w], axis=-1))

    start = time()
    skeleton = Skeleton()
    (
        log_lines,
        roots_X,
        roots_Y,
        roots_Z,
        angles_X,
        angles_Y,
        angles_Z,
        X,
        Y,
        Z,
    ) = initialize(x, y, skeleton, 0.001)
    end = time()
    print(f"Initializing: {end - start}")
    save("results/c3_c4.json", np.stack([X, Y, Z], axis=-1))

    if torch.cuda.is_available():
        device = torch.device("cuda")
    else:
        device = torch.device("cpu")

    # Set the max number of threads for CPU ops. It may boost the performance.
    num = 8
    torch.set_num_threads(num)
    torch.set_num_interop_threads(num)

    start = time()

    roots_XYZ = np.stack([roots_X, roots_Y, roots_Z], axis=-1)
    angles_XYZ = np.stack([angles_X, angles_Y, angles_Z], axis=-1)
    xy = np.stack([x, y], axis=-1)
    w = w[..., None]

    log_lines = torch.from_numpy(log_lines)
    roots_XYZ = torch.from_numpy(roots_XYZ)
    angles_XYZ = torch.from_numpy(angles_XYZ)
    xy = torch.from_numpy(xy)
    w = torch.from_numpy(w)

    dtype = torch.float64
    # dtype = torch.float32

    optimization = Optimization(max_iterations=1000, lr=0.1)
    log_lines = log_lines.to(device=device, dtype=dtype, non_blocking=True)
    roots_XYZ = roots_XYZ.to(device=device, dtype=dtype, non_blocking=True)
    angles_XYZ = angles_XYZ.to(device=device, dtype=dtype, non_blocking=True)
    xy = xy.to(device=device, dtype=dtype, non_blocking=True)
    w = w.to(device=device, dtype=dtype, non_blocking=True)

    log_lines, roots_XYZ, angles_XYZ, XYZ = optimization(
        log_lines, roots_XYZ, angles_XYZ, xy, w, skeleton
    )
    end = time()
    print(f"Optimizing: {end - start}")
    save("results/c4_c5.json", XYZ)

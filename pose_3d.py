import torch
from torch import Tensor
from torch.nn import Module
from torch.optim import SGD

from skeleton import Skeleton


class Optimization(object):
    def __init__(
        self,
        max_iterations: int = 1000,
        lr: float = 0.1,
        r1: float = 0.001,
        r2: float = 0.1,
    ):
        self.max_iterations = max_iterations
        self.lr = lr
        self.r1 = r1
        self.r2 = r2
        self.eps = 1e-10

    def __call__(
        self,
        log_lines: Tensor,
        roots_XYZ: Tensor,
        angles_XYZ: Tensor,
        xy: Tensor,
        w: Tensor,
        skeleton: Skeleton,
    ):
        T, N, _ = xy.shape
        XYZ = torch.zeros(T, N, 3, dtype=xy.dtype, device=xy.device)

        log_lines.requires_grad_()
        roots_XYZ.requires_grad_()
        angles_XYZ.requires_grad_()
        optimizer = SGD([log_lines, roots_XYZ, angles_XYZ], lr=self.lr)

        for _ in range(self.max_iterations):
            optimizer.zero_grad()

            XYZ = torch.zeros_like(XYZ)
            XYZ[:, 0] = roots_XYZ

            for i in range(skeleton.n_bones):
                s, e, l = skeleton.structure[i]

                line = log_lines[l].exp()

                XYZ_s = XYZ[:, s]

                angle_XYZ = angles_XYZ[:, i]
                angle_norm = angle_XYZ.norm(p=2, dim=-1, keepdim=True)
                angle_XYZ = angle_XYZ / (angle_norm + self.eps)

                XYZ_e = XYZ_s + line * angle_XYZ

                XYZ[:, e] = XYZ_e

            XY, _ = XYZ.split([2, 1], dim=-1)
            XYZ_0, XYZ_1 = XYZ[:-1], XYZ[1:]

            loss_0 = (w * (XY - xy) ** 2).mean()
            reg_1 = log_lines.exp().sum()
            reg_2 = ((XYZ_1 - XYZ_0) ** 2).mean()

            loss = loss_0 + self.r1 * reg_1 + self.r2 * reg_2
            loss.backward()

            optimizer.step()

        log_lines = log_lines.detach()
        roots_XYZ = roots_XYZ.detach()
        angles_XYZ = angles_XYZ.detach()
        XYZ = XYZ.detach()
        return log_lines, roots_XYZ, angles_XYZ, XYZ

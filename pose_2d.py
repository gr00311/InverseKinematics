import numpy as np


def normalize(x, y):
    assert x.shape == y.shape

    mu_x, mu_y = x.mean(), y.mean()

    xy = np.stack([x, y], axis=-1)
    sigma = xy.std()
    if sigma < 1e-5:
        sigma = 1e-5

    x = (x - mu_x) / sigma
    y = (y - mu_y) / sigma

    return x, y


def prune(x, y, w, indexes, threshold):
    assert x.shape == y.shape == w.shape

    w_ = w[:, indexes].mean(axis=-1, keepdims=True)

    c = w_ < threshold
    c = np.resize(c, w.shape)

    x[c] = 0.0
    y[c] = 0.0
    w[c] = 0.0

    return x, y, w


def interpolate(x, y, w, threshold):
    assert x.shape == y.shape == w.shape

    T, N = x.shape

    x_ = np.zeros_like(x)
    y_ = np.zeros_like(y)
    for t in range(T):
        for i in range(N):
            w__ = w[t, i]
            x__ = x[t, i]
            y__ = y[t, i]

            s_x = w__ * x__
            s_y = w__ * y__
            s_w = w__

            delta = 0
            while s_w < threshold:
                change = False
                delta += 1

                t_ = t + delta
                if t_ < T:
                    w__ = w[t_, i]
                    x__ = x[t_, i]
                    y__ = y[t_, i]

                    s_x = s_x + w__ * x__
                    s_y = s_y + w__ * y__
                    s_w = s_w + w__

                    change = True

                t_ = t - delta
                if t_ >= 0:
                    w__ = w[t_, i]
                    x__ = x[t_, i]
                    y__ = y[t_, i]

                    s_x = s_x + w__ * x__
                    s_y = s_y + w__ * y__
                    s_w = s_w + w__

                    change = True

                if not change:
                    break

            if s_w <= 0.0:
                s_w = 1e-10

            x_[t, i] = s_x / s_w
            y_[t, i] = s_y / s_w
    return x_, y_

```shell script
conda create -y -n MYENV python=3.7 pip
conda activate MYENV
conda install -y pytorch torchvision torchaudio cudatoolkit=10.2 -c pytorch
pip install h5py
```
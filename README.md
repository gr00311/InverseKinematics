# Inverse Kinematics

This tool is developed to produce a sequence of 3D skeletons given a sequence of 2D poses for the [CONTENT4ALL Project](http://content4all-project.eu/) (an European Union’s Horizon 2020 research and innovation funded project).


# Requirements
* Python 3.7 
* Anaconda

# Installation

Create new environment using conda:

```shell
conda create -n ik-env conda pip python=3.7
```

Activate the environment:

```shell
conda activate ik-env
```

Install Requirements

```shell
conda install -y pytorch torchvision torchaudio cudatoolkit=10.2 -c pytorch
pip install h5py
```


# Inference

Activate the environment:

```shell
conda activate ik-env
```

As shown in [main.py](main.py),
1. Prepare your OpenPose data as an (T, N, 2) array.
2. Run the five steps: normalize(), prune(), interpolate(), initialize(), optimization().
3. Re-pack the (T, N, 3) array the way you want!

## Authors

This software was developed by University of Surrey and integrated by FINCONS GROUP AG within the CONTENT4ALL Project.  
For any further information, please contact [content4all-dev@finconsgroup.com](mailto:content4all-dev@finconsgroup.com) or [g.rochette@surrey.ac.uk](mailto:g.rochette@surrey.ac.uk).

# License

This work is the property of the University of Surrey, as shown [here](LICENSE).
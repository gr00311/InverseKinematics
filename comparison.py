import numpy as np

from utils import load


if __name__ == "__main__":
    a, b = "results/c4_c5.json", "results/d4_d5.json"
    # a, b = "results/d4_c5.json", "results/d4_d5.json"
    # a, b = "results/c4_c5.json", "results/d4_c5.json"

    a, b = load(a), load(b)
    c = a - b
    mean, std = c.mean(axis=(0, 1)), c.std(axis=(0, 1))
    m, M = c.min(axis=(0, 1)), c.max(axis=(0, 1))

    print(a.shape, b.shape)
    print("mean/std")
    print(np.stack([mean, std], axis=1))
    print("min/max")
    print(np.stack([m, M], axis=1))

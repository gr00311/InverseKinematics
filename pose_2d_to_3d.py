import numpy as np


def compute_angle(X_s, Y_s, Z_s, x_e, y_e, line):
    foo = line ** 2 - (x_e - X_s) ** 2 - (y_e - Y_s) ** 2
    foo1 = X_s ** 2 - 2 * X_s * x_e + Y_s ** 2 - 2 * Y_s * y_e + x_e ** 2 + y_e ** 2
    foo2 = (1 / foo1) ** (1 / 2)
    foo3 = (
        Y_s ** 3 / foo1
        + line * Y_s * foo2
        - line * y_e * foo2
        + (X_s ** 2 * Y_s) / foo1
        + (Y_s * x_e ** 2) / foo1
        + (Y_s * y_e ** 2) / foo1
        - (2 * Y_s ** 2 * y_e) / foo1
        - (2 * X_s * Y_s * x_e) / foo1
    )
    foo4 = (
        Y_s ** 3 / foo1
        - line * Y_s * foo2
        + line * y_e * foo2
        + (X_s ** 2 * Y_s) / foo1
        + (Y_s * x_e ** 2) / foo1
        + (Y_s * y_e ** 2) / foo1
        - (2 * Y_s ** 2 * y_e) / foo1
        - (2 * X_s * Y_s * x_e) / foo1
    )
    x_1 = -(X_s * y_e - Y_s * x_e - X_s * foo3 + x_e * foo3) / (Y_s - y_e)
    x_2 = -(X_s * y_e - Y_s * x_e - X_s * foo4 + x_e * foo4) / (Y_s - y_e)
    y_1 = (
        Y_s ** 3 / foo1
        + line * Y_s * foo2
        - line * y_e * foo2
        + (X_s ** 2 * Y_s) / foo1
        + (Y_s * x_e ** 2) / foo1
        + (Y_s * y_e ** 2) / foo1
        - (2 * Y_s ** 2 * y_e) / foo1
        - (2 * X_s * Y_s * x_e) / foo1
    )
    y_2 = (
        Y_s ** 3 / foo1
        - line * Y_s * foo2
        + line * y_e * foo2
        + (X_s ** 2 * Y_s) / foo1
        + (Y_s * x_e ** 2) / foo1
        + (Y_s * y_e ** 2) / foo1
        - (2 * Y_s ** 2 * y_e) / foo1
        - (2 * X_s * Y_s * x_e) / foo1
    )

    hypangles_X = []
    hypangles_Y = []
    hypangles_Z = []

    c = foo < 0.0
    foo[c] = 0.0

    c = ~(np.isfinite(x_1 * x_2 * y_1 * y_2))
    x_1[c] = x_e[c]
    x_2[c] = x_e[c]
    y_1[c] = y_e[c]
    y_2[c] = y_e[c]

    hypangles_X += [x_e - X_s]
    hypangles_Y += [y_e - Y_s]
    hypangles_Z += [np.zeros_like(x_e)]

    hypangles_X += [x_e - X_s]
    hypangles_Y += [y_e - Y_s]
    hypangles_Z += [-(foo ** (1 / 2))]

    hypangles_X += [x_e - X_s]
    hypangles_Y += [y_e - Y_s]
    hypangles_Z += [foo ** (1 / 2)]

    hypangles_X += [x_1 - X_s]
    hypangles_Y += [y_1 - Y_s]
    hypangles_Z += [np.zeros_like(x_1)]

    hypangles_X += [x_2 - X_s]
    hypangles_Y += [y_2 - Y_s]
    hypangles_Z += [np.zeros_like(x_2)]

    angle_X = np.ones_like(x_e)
    angle_Y = np.ones_like(x_e)
    angle_Z = np.zeros_like(x_e)

    inf = np.resize(np.inf, X_s.shape)
    min_loss = np.copy(inf)
    eps = 1e-10
    for hypangle_X, hypangle_Y, hypangle_Z in zip(
        hypangles_X, hypangles_Y, hypangles_Z
    ):
        hypangle_norm = (hypangle_X ** 2 + hypangle_Y ** 2 + hypangle_Z ** 2) ** (1 / 2)

        X_e = X_s + line * hypangle_X / (hypangle_norm + eps)
        Y_e = Y_s + line * hypangle_Y / (hypangle_norm + eps)
        Z_e = Z_s + line * hypangle_Z / (hypangle_norm + eps)

        loss = (X_e - x_e) ** 2 + (Y_e - y_e) ** 2

        c = ~np.isfinite(loss)
        loss[c] = inf[c]

        c = loss < min_loss
        min_loss[c] = loss[c]
        angle_X[c] = hypangle_X[c]
        angle_Y[c] = hypangle_Y[c]
        angle_Z[c] = hypangle_Z[c]

    return angle_X, angle_Y, angle_Z


def initialize(x, y, skeleton, epsilon):
    assert x.shape == y.shape
    T, N = x.shape

    # Collect the lengths of the "lines", e.g. bones,
    # but for example, the left and right forearms are considered as a single bone.
    lines = {}
    for i in range(skeleton.n_bones):
        s, e, l = skeleton.structure[i]
        x_s, y_s = x[:, s], y[:, s]
        x_e, y_e = x[:, e], y[:, e]
        line = ((x_e - x_s) ** 2 + (y_e - y_s) ** 2) ** (1 / 2)
        if l not in lines:
            lines[l] = line
        else:
            line_ = lines[l]
            lines[l] = np.concatenate([line_, line], axis=0)

    # Compute the 0.5-quantile of the "lines" lengths.
    lines = {l: np.quantile(line, q=0.5) for l, line in lines.items()}
    lines = np.stack([lines[i] for i in range(len(lines))], axis=0)
    log_lines = np.log(lines)

    roots_X = x[:, 0]
    roots_Y = y[:, 0]
    roots_Z = np.zeros_like(roots_X)

    e_X = (np.random.rand(*roots_X.shape) - 0.5) * 2 * epsilon
    e_Y = (np.random.rand(*roots_Y.shape) - 0.5) * 2 * epsilon
    e_Z = (np.random.rand(*roots_Z.shape) - 0.5) * 2 * epsilon

    roots_X = roots_X + e_X
    roots_Y = roots_Y + e_Y
    roots_Z = roots_Z + e_Z

    angles_X = np.zeros((T, skeleton.n_bones), dtype=x.dtype)
    angles_Y = np.zeros((T, skeleton.n_bones), dtype=x.dtype)
    angles_Z = np.zeros((T, skeleton.n_bones), dtype=x.dtype)

    X = np.zeros((T, skeleton.n_points), dtype=x.dtype)
    Y = np.zeros((T, skeleton.n_points), dtype=x.dtype)
    Z = np.zeros((T, skeleton.n_points), dtype=x.dtype)

    X[:, 0] = roots_X
    Y[:, 0] = roots_Y
    Z[:, 0] = roots_Z

    eps = 1e-10

    for i in range(skeleton.n_bones):
        s, e, l = skeleton.structure[i]
        line = lines[l]

        X_s = X[:, s]
        Y_s = Y[:, s]
        Z_s = Z[:, s]

        x_e = x[:, e]
        y_e = y[:, e]

        angle_X, angle_Y, angle_Z = compute_angle(X_s, Y_s, Z_s, x_e, y_e, line)

        angle_X[~np.isfinite(angle_X)] = 0.0
        angle_Y[~np.isfinite(angle_Y)] = 0.0
        angle_Z[~np.isfinite(angle_Z)] = 0.0

        c = (angle_X == 0.0) & (angle_Y == 0.0) & (angle_Z == 0.0)
        angle_X[c] = 1.0
        angle_Y[c] = 1.0
        angle_Z[c] = 1.0

        angle_Z = np.abs(angle_Z)
        angle_Z = angle_Z + 0.001

        angle_norm = np.sqrt(angle_X ** 2 + angle_Y ** 2 + angle_Z ** 2)

        angle_X = angle_X / (angle_norm + eps)
        angle_Y = angle_Y / (angle_norm + eps)
        angle_Z = angle_Z / (angle_norm + eps)

        X_e = X_s + line * angle_X
        Y_e = Y_s + line * angle_Y
        Z_e = Z_s + line * angle_Z

        angles_X[:, i] = angle_X
        angles_Y[:, i] = angle_Y
        angles_Z[:, i] = angle_Z

        X[:, e] = X_e
        Y[:, e] = Y_e
        Z[:, e] = Z_e

    return log_lines, roots_X, roots_Y, roots_Z, angles_X, angles_Y, angles_Z, X, Y, Z
